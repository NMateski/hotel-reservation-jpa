package com.nikolamateski.hotelreservationsystemjpa.hotel;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class HotelDTO {

    public Integer id;
    public String name;

}
