package com.nikolamateski.hotelreservationsystemjpa.hotel;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class HotelRequest {

    public String name;
}
