package com.nikolamateski.hotelreservationsystemjpa.hotel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface HotelRepository extends JpaRepository<Hotel, Integer>, JpaSpecificationExecutor<Hotel> {


    @Query(nativeQuery = true, value = "SELECT * FROM hotel WHERE name = :name LIMIT 1")
    Optional<Hotel> findFirstByNameNative(String name);
}
