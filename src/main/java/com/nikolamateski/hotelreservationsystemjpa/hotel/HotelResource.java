package com.nikolamateski.hotelreservationsystemjpa.hotel;

import com.nikolamateski.hotelreservationsystemjpa.util.VoidDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/hotels")
@RequiredArgsConstructor
public class HotelResource {

    final public HotelService hotelService;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDTO create(@RequestBody final HotelRequest request) {
        return hotelService.create(request);
    }

    @PutMapping(path = "/{hotelId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDTO update(@PathVariable("hotelId") final Integer hotelId, @RequestBody final HotelRequest request) {
        return hotelService.update(hotelId, request);
    }

    @DeleteMapping(path = "/{hotelId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(@PathVariable("hotelId") final Integer hotelId) {
        hotelService.delete(hotelId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidDTO(true));
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<HotelDTO> findPage(
            @RequestParam(value = "name", required = false) final String name, Pageable pageable) {
        return hotelService.findPage(new HotelSearchRequest(name, pageable));
    }

    @GetMapping(path = "/{hotelId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDTO findById(@PathVariable("hotelId") final Integer hotelId) {
        return hotelService.findById(hotelId);
    }
}
