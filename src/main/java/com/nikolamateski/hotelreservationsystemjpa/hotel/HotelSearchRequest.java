package com.nikolamateski.hotelreservationsystemjpa.hotel;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class HotelSearchRequest {

    public String name;
    public Pageable pageable;

    public HotelSearchRequest(String name, Pageable pageable) {
        this.name = name;
        this.pageable = pageable;
    }

    public Specification<Hotel> generateSpecification() {
        Specification<Hotel> categorySpecification = Specification.where(null);

        if (StringUtils.hasText(name)) {
            categorySpecification = categorySpecification.and(HotelSpecifications.byNameLiteralEquals(name));
        }

        return categorySpecification;
    }
}
