package com.nikolamateski.hotelreservationsystemjpa.hotel;

import com.nikolamateski.hotelreservationsystemjpa.exception.ResourceValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor // create constructor for all final properties
public class HotelService {

    final private HotelRepository hotelRepository;

    public HotelDTO create(final HotelRequest hotelRequest) {
        var doesHotelExist = hotelRepository.findFirstByNameNative(hotelRequest.name).isPresent();
        if (doesHotelExist) {
            throw new RuntimeException("The hotel already exists!");
        }
        var hotel = new Hotel(hotelRequest.name);
        return hotelRepository.save(hotel).toDTO();
    }

    public HotelDTO update(final Integer hotelId, final HotelRequest hotelRequest) {
        var hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceValidationException("Hotel with ID " + hotelId + " not found!"));
        hotel.name = hotelRequest.name;
        return hotelRepository.save(hotel).toDTO();
    }

    public void delete(final Integer hotelId) {
        var hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceValidationException("Hotel with ID " + hotelId + " not found!"));
        hotelRepository.delete(hotel);
    }

    public Page<HotelDTO> findPage(final HotelSearchRequest request) {
        return hotelRepository.findAll(request.generateSpecification(), request.pageable)
                .map(Hotel::toDTO);
    }

    public HotelDTO findById(final Integer hotelId) {
        var hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceValidationException("Hotel with ID " + hotelId + " not found!"));

        return hotel.toDTO();
    }
}
