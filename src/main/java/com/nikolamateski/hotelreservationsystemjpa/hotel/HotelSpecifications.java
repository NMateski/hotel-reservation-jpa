package com.nikolamateski.hotelreservationsystemjpa.hotel;

import org.springframework.data.jpa.domain.Specification;

public class HotelSpecifications {

    public HotelSpecifications() {
    }

    public static Specification<Hotel> byNameLiteralEquals(final String name) {
        return ((root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("name")), name.toLowerCase()));
    }
}
