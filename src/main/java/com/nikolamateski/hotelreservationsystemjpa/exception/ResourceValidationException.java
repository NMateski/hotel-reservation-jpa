package com.nikolamateski.hotelreservationsystemjpa.exception;

public class ResourceValidationException extends RuntimeException{

    public ResourceValidationException(String message) {
        super(message);
    }
}
