package com.nikolamateski.hotelreservationsystemjpa.util;

public class ErrorDTO {

    public String message;

    public ErrorDTO(String message) {
        this.message = message;
    }
}
