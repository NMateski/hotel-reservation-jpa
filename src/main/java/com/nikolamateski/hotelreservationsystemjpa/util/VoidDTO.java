package com.nikolamateski.hotelreservationsystemjpa.util;

public class VoidDTO {

    public boolean success;

    public VoidDTO(boolean success) {
        this.success = success;
    }
}
